# TE-MPE-CB: Reliability, Availability and Machine Learning Project Documentation

This site contains the documentation for reliability, availability and 
machine learning projects in the TE-MPE-CB section.

## Getting Started
Here are some helpful documentations to getting started.

### Website Information
The source files for this website can be found in this [GitLab repository](https://gitlab.cern.ch/cobermai/project-documentations/-/tree/master).

Click [here](https://how-to.docs.cern.ch/new/) if you want to create your own
Markdown-based static documentation site with MkDocs from scratch.


###1. Execute notebooks with SWAN:
1. Go to SWAN [https://swan.cern.ch](https://swan.cern.ch) 
(In case you haven’t activated CERNbox yet, first go to [https://cernbox.cern.ch](https://cernbox.cern.ch)  )
2. Clone a git repository https://gitlab.cern.ch/clicml.git into the "SWAN_projects" of your cernbox
3. Execute the chosen notebook, cell by cell

####1.1 Execute notebooks with SWAN using a GPU from outside CERN:
Please find below the instructions to use the SWAN instance quipped with GPUs:
1a. Connect to [https://swan-k8s.cern.ch](https://swan-k8s.cern.ch) with your CERN username and password
1b. The GPU SWAN instance at https://swan-k8s.cern.ch is still not accessible from outside the CERN network. 
2. In the web form, start your session with "Cuda 10 Python3" as Software stack.
 
3. Once you are in your SWAN session, you can create projects and notebooks as usual in SWAN. The difference is that there are some libraries in your environment that have been compiled with GPU support. For example, if you use TensorFlow from a Python notebook, it will offload computations to the GPU. You also have the nvcc compiler available which you can use from the SWAN terminal.

####1.2 Install packages in SWAN:
A typical case is the installation of Python packages, which requires to run pip from a SWAN terminal:
* pip install --user package_name

If this fails because you are trying to install an updated version of a package that already exists in CVMFS, you will need to add the --upgrade flag.
Then, it would be necessary to add the local installation path to PYTHONPATH, by creating a bash startup script that configures that variable (don't forget to call this startup script in the session configuration menu)(use current python version in script):
* export PYTHONPATH=$CERNBOX_HOME/.local/lib/python3.5/site-packages:$PYTHONPATH

As a result of performing the aforementioned steps, the package will be installed on your CERNBox and it will be picked by any notebook you open after that. Since the package is on your CERNBox, it will be also available in any new session you start in SWAN.

###2. How to connect to CERN from outside
####2.1 Remote desktop to CERN
1.	Open windows remote
2.	cernts.cern.ch
3.	Log into CERN\username

####2.2 Conect your Browser with SSH Proxy
1. In a shell, open an SSH tunnel: ssh -D6789 username@lxplus.cern.ch
2. In Firefox: Preferences->advanced->network->settings. Tick the radio button "Manual Proxy Configuration", SOCKS Host set to 127.0.0.1 and Port to 6789
 
###3. How to Submit Jobs to HTCondor
Guide: [https://batchdocs.web.cern.ch/local/quick.html](https://batchdocs.web.cern.ch/local/quick.html)

Example submission file:

```python
executable            = htc_run.sh
arguments             = $(ClusterId) $(ProcId) $(settings)
output                = output/htc_run.$(ClusterId).$(ProcId).out
error                 = error/htc_run.$(ClusterId).$(ProcId).err
log                   = log/htc_run.$(ClusterId).log
request_CPUs          = 2
request_GPUs          = 1
requirements          = regexp("V100", TARGET.CUDADeviceName)
+JobFlavour           = "workday"
+AccountingGroup      = "group_u_TE.mpe"
queue 6
```

Example shell script:

```python
echo "activate environment"
python -m virtualenv myvenv
source /afs/cern.ch/user/c/cobermai/Desktop/afs_work/miniconda3/bin/activate base

echo "start script"
python /afs/cern.ch/user/c/cobermai/Desktop/afs_work/PycharmProjects/quench-prediction/src/main.py --output=$3 --settings=$3.json --ProcId=$2
```

####3.1 Install package to environment
1. In a shell, open an SSH tunnel: ssh -D6789 username@lxplus.cern.ch
2. cd /afs/cern.ch/user/c/cobermai/Desktop/afs_work/miniconda3/bin
3. source activate base
4. conda install -c conda-forge packagename

###4. How to Set Up Pycharm

###5. How to use Openstack 
Guide: [https://clouddocs.web.cern.ch/gpu/README.html ](https://clouddocs.web.cern.ch/gpu/README.html)

Connect to [instance](https://openstack.cern.ch/project/instances/) via remote desktop

Open new openstack instance:
1.	[https://openstack.cern.ch/project/instances/](https://openstack.cern.ch/project/instances/)
2.	New instance
3.	landb-mainuser: cobermai
4.	landb-responsible: machine-protection-studies

###5. Python Style Guidline
In order to make our code comparable, please write it like this:
```python
#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""This module's docstring summary line.
This is a multi-line docstring. Paragraphs are separated with blank lines.
Lines conform to 79-column limit.
Module and packages names should be short, lower_case_with_underscores.
Notice that this in not PEP8-cheatsheet.py
Seriously, use flake8. Atom.io with https://atom.io/packages/linter-flake8
is awesome!
See http://www.python.org/dev/peps/pep-0008/ for more PEP-8 details
"""


import os  # STD lib imports first
import sys  # alphabetical

import some_third_party_lib  # 3rd party stuff next
import some_third_party_other_lib  # alphabetical

import local_stuff  # local stuff last
import more_local_stuff
import dont_import_two, modules_in_one_line  # IMPORTANT!
from pyflakes_cannot_handle import *  # and there are other reasons it should be avoided # noqa
# Using # noqa in the line above avoids flake8 warnings about line length!


_a_global_var = 2  # so it won't get imported by 'from foo import *'
_b_global_var = 3

A_CONSTANT = 'ugh.'


# 2 empty lines between top-level funcs + classes
def naming_convention():
    """Write docstrings for ALL public classes, funcs and methods.
    Functions use snake_case.
    """
    if x == 4:  # x is blue <== USEFUL 1-liner comment (2 spaces before #)
        x, y = y, x  # inverse x and y <== USELESS COMMENT (1 space after #)
    c = (a + b) * (a - b)  # operator spacing should improve readability.
    dict['key'] = dict[0] = {'x': 2, 'cat': 'not a dog'}


class NamingConvention(object):
    """First line of a docstring is short and next to the quotes.
    Class and exception names are CapWords.
    Closing quotes are on their own line
    """

    a = 2
    b = 4
    _internal_variable = 3
    class_ = 'foo'  # trailing underscore to avoid conflict with builtin

    # this will trigger name mangling to further discourage use from outside
    # this is also very useful if you intend your class to be subclassed, and
    # the children might also use the same var name for something else; e.g.
    # for simple variables like 'a' above. Name mangling will ensure that
    # *your* a and the children's a will not collide.
    __internal_var = 4

    # NEVER use double leading and trailing underscores for your own names
    __nooooooodontdoit__ = 0

    # don't call anything (because some fonts are hard to distiguish):
    l = 1
    O = 2
    I = 3

    # some examples of how to wrap code to conform to 79-columns limit:
    def __init__(self, width, height,
                 color='black', emphasis=None, highlight=0):
        if width == 0 and height == 0 and \
           color == 'red' and emphasis == 'strong' or \
           highlight > 100:
            raise ValueError('sorry, you lose')
        if width == 0 and height == 0 and (color == 'red' or
                                           emphasis is None):
            raise ValueError("I don't think so -- values are %s, %s" %
                             (width, height))
        Blob.__init__(self, width, height,
                      color, emphasis, highlight)

    # empty lines within method to enhance readability; no set rule
    short_foo_dict = {'loooooooooooooooooooong_element_name': 'cat',
                      'other_element': 'dog'}

    long_foo_dict_with_many_elements = {
        'foo': 'cat',
        'bar': 'dog'
    }

    # 1 empty line between in-class def'ns
    def foo_method(self, x, y=None):
        """Method and function names are lower_case_with_underscores.
        Always use self as first arg.
        """
        pass

    @classmethod
    def bar(cls):
        """Use cls!"""
        pass

# a 79-char ruler:
# 34567891123456789212345678931234567894123456789512345678961234567897123456789

"""
Common naming convention names:
snake_case
MACRO_CASE
camelCase
CapWords
"""

# Newline at end of file
```



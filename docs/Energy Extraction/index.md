# Energy Extraction

This project is a reliability study of the HL-LHC Energy Extraction systems. The upgraded version involves several new concepts and a new technology that has not been used earlier at CERN (vacuum interrupters). The objective of this project is to quantify the likelihood of critical failures through modelling and simulation methods. Any period of EE system malfunctioning might expose the corresponding magnet to potentially severe outcomes of quenches, rendering it vulnerable to irreversible damage. Magnet replacement is in the S6 category of the LHC risk matrix as it requires a significant amount of time and resources, causing long delays and sharply reducing the overall availability of the entire collider. High, near-perfect reliability whenever quench occurs is therefore of paramount importance to ensure an appropriate level of protection and to meet the adopted targets. Simulations performed within this study include investigation of the failure rates estimations for individual components, redundant aspects of the new system, planned monitoring and maintenance strategies. The models are developed and evaluated in AvailSim4 - a simulation software developed in-house specifically to address the needs of availability and reliability studies in accelerator-related systems.

![HL-LHC Energy Extraction system schema](images/ees_schema.png)

## Minutes

### 2020
* [20201021: 1st meeting of the Energy Extraction Reliability Study](minutes/EE_reliability_1_201021.docx)
    - [outline of the schema](minutes/EE_reliability_1_201021.pptx)
* [20201029: 2nd meeting of the Energy Extraction Reliability Study](minutes/EE_reliability_2_201029.docx)
    - [general diagram](minutes/EE_reliability_2_201029.odp)
* [E-mail message](minutes/EE_reliability_SM18_study.docx)

### 2021
* [20210211: Meeting regarding access to historical EE systems data](minutes/EE_historicaldata_210211_TC.docx)
* [20210215: Meeting - practical aspects of querying and accessing historical EE systems data](minutes/EE_historicaldata_210215_TC.docx)
* [20210302: Meeting with B. Panev](minutes/EE_reliability_3_210302_TC.docx)
* [20210319: Meeting with B. Panev](minutes/EE_reliability_4_210319_TC.docx)
    - [presentation](minutes/EE_reliability_4_210319_presentation_final.pptx)
* [20210428: Meeting with B. Panev](minutes/EE_reliability_5_210428_TC.docx)
    - [presentation](minutes/EE_reliability_5_210428_presentation.pptx)

************************************************************************************************************************
************************************************** Minutes *************************************************************
************************************************************************************************************************

We discussed our first results with Gerard.

1. Gerard is convinced it is impossible to predict quenches. For sure not with Vibration. If possible, then with the
Voltage.
2. Our study is interesting to gain further understanding. We should look at existing work and see if ML can bring us
beyond. Especially interesting could be a study of the location and anomalies.
3. There has been prior ML work, where they produced images out of vibrations.
4. For the features we should rather focus on amplitude than frequency. The frequency changes comes from the structure
itself. E.g. the 3 important frequency ranges in our spectrum can probably be explained with the diameter of the magnet
and the vibration speed.
5. We should also look at flux jumps (0-4kA) as they do harm to the PC and possibly the beam.
6. Next step is to analyze furhter data and then do a first presentation in about 3 weeks.

************************************************************************************************************************
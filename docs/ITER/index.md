# ITER
Machine learning study of Discharge Prediction in Nuclear Fusion Reactors. 
This project is a collaboration between TU-Graz and ITER. CERN is only providing the CLIC ML Framework.

## Overview
Electrical discharges in vacuum/low pressure environments are one of the most critical restrictions in the 
efficiency/reliability of the three 1 MeV - 16 MW Neutral Beam Injectors (NBI) which will be installed in the 
International Thermonuclear Experimental Reactor (ITER). These NBIs are fundamental to attain the breakeven condition 
in the ITER thermonuclear Plasma.

The ITER NBIs are complex systems, with very demanding ratings, much larger than any other NBI ever built so far 
(2 times the acceleration voltage, 10 times the negative ion current extracted, pulse duration 1 h). 
For this reason, the Neutral Beam Test Facility was built in Padova at the Consorzio RFX Laboratory. 
This facility consists of two experiments: (1) MITICA, aimed at testing the NBI system to demonstrate the feasibility 
to obtain 16 MW neutral beam @ 1 MeV, and (2) SPIDER aimed at optimizing the negative ion source designed to demonstrate 
all ITER source requirements.

The goal of the project is to analyze the existing data of the Consorzio RFX experiments in order to develop 
data driven models for discharge prediction. 

## Minutes
* [20210319: Kick off](minutes/20210319_Kick_off.txt) [(paper)](documentation/Pilan_2020.pdf) 
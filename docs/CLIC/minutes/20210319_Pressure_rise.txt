************************************************************************************************************************
************************************************** MINUTES *************************************************************
************************************************************************************************************************


************************************************************************************************************************
************************************************** EMAILS **************************************************************
************************************************************************************************************************

Hi Lee,

You should have a meeting today to discuss experiments I believe.
I just want to emphasize something again, the use of the word precursor etc.
You already replied to but maybe it will help the discussion with Nuria :-D

Here is a small summary:
1) we see a rise of pressure before a breakdown
2) there is a delay of up to several seconds between the rise of pressure and the bd
3) it means hundreds (x seconds * 50 Hz) of healthy breakdowns occur at "high" pressure before the bd

Our conclusion is, pressure only is not the only reason but either:
1) something creates that rise of pressure and the bd, but the rise of pressure is not the reason for the bd
2) the rise of pressure generates some favorable condition for the bd, but some transient is needed

The second point could be that the system need a few seconds to turn that gas release into some plasma,
plasma which then make a shortcurt/spark/bd.

Would you know:
1) the volume of the pipe? (length x diameter ? or square surface for the wave guide ?)
2) the volume of the cavity
3) the type of gas which is released ?
4) some way to measure the heat absorption of that gas ? (on the signal reflect, in the cooling, just by having an idea
of how hot is the device?)

Sorry for that long email !
Thank you again !
Thomas

************************************************************************************************************************

Hi Thomas

Thanks for the email, the meeting started at 10h00 so the timing was perfect! It looks like we might get a week to run
some tests next month but Nuria would like to have a meeting to discuss things first. I'll try to find a time when
everyone is available and send out an invitation soon so we can organise the details.

I think your first conclusion i.e. "something creates that rise of pressure and the bd, but the rise of pressure
is not the reason for the bd" is the correct one here.

When the arc forms, it seems to happen entirely within a single pulse and the RF pulse is very short (50-200ns)
so I don't think the system needs a few seconds to establish the arc as the gas would have sufficient time to cool and
disperse between pulses (about 20ms). I have linked a thesis here which has a description of what we think happens
during breakdown in section 2.3 on page 34.


For the questions:
We use WR90 waveguide, so the internal dimensions are 22.86mm x 10.16 mm. The total length varies depending on what
components we have installed in the waveguide network.
The accelerating structure dimensions vary depending on the design. The structure which you have the data for has a
volume of around 60,000-70,000mm^3. I can link you to a spreadsheet/CAD file on EDMS with the exact dimensions in
case you want them.
We used to have an RGA installed so I've attached a few plots. One of them shows the spikes from breakdowns,
the other is from a time when we modified the pulse length (this usually results in some vacuum activity/outgassing
as the surface heats up more with longer pulses). We mostly measured water/hyrogen. During breakdowns I guess a lot of
the ions get captured and accelerated by the fields too.
For the heat absorption of that gas, I'm not sure if we could measure that. We have integrated the energy in the RF
signals during breakdown, but it's very difficult to distinguish between the places the energy can go.
It depends on the breakdown location in the structure (i.e. how much of the energy is dissipated as ohmic heating
in the cavity walls) and how much energy is deposited in accelerating ions/electrons (we saturate the Faraday cup and
don't have a spectrometer so this is basically impossible to estimate). What I can say though, is that field emission
and breakdowns can create very high local temperatures due to something called the Nottingham effect. Our colleagues
simulations predict that during breakdowns the emission site can reach a few thousand degrees.  If you look at the size
of the breakdown site however, only a few mJ of energy is needed to melt that amount of copper and when we integrate the
reflected RF signal we see that virtually all of the energy (around 10J) is in the reflected RF pulse. So in any case it
seems like we don't deposit a lot of energy in the actual arc/plasma itself. Good question!

Regards
Lee

************************************************************************************************************************
Hi Lee,

Thank you very much for your detailed answer and the good news about the experiment !

I tried to thing about the system a bit, but I still don't know how to explain that delay between the
pressure spike and the break down...
I do agree that a spark is generated within one pulse (~="no time"), but why is not produced earlier with
respect to the spike of pressure?
I guess you need to have the right conditions to produce that spark, maybe the gas pressure / temperature /
flow (when the spike is rising, I guess the flow is high, while when we have the exponential decay due to the pump,
maybe the  flow is weaker). Maybe the release of gas has to propagate (in those few seconds) up to a particular point...
Or the gas has nothing to do with it !

We're looking forward to that meeting !
Thank you again !
Thomas
************************************************************************************************************************
Hi Thomas

Ah, I understand what you mean now. Unfortunately I don't have an answer for that,
I'm not sure why it happens on a specific pulse. My current interpretation is that breakdown is stochastic and
that whatever happens during the pressure spike just makes breakdown much more likely to occur but not necessarily
guaranteed. I have no idea if the gas or local pressure affects it or gives it a more favourable condition so you could
be right.

Even with the pressure spikes, we have millions of normal RF pulses before we see one so that also seems to be some kind
of dynamic and fairly stochastic process. I'm not sure why either the gas spikes or the breakdowns happen on a specific
pulse (maybe something to do with cyclic stress/fatigue in the grains).

Regards
Lee
************************************************************************************************************************
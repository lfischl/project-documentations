# CLIC
Machine learning study of CLIC RF breakdowns. All our code is available in this
[GitLab repository](https://gitlab.cern.ch/machine-protection-ml/mlframework).
Our To-Dos and next steps are discussed 
[here](https://docs.google.com/spreadsheets/d/1FPKwkvzl6WROZmj36cw_ujmlYRujikp5OMjryt8syEY/edit?usp=sharing).

## Overview
Radio Frequency (RF) breakdowns are one of the most prevalent limits in RF cavities for particle accelerators. 
During a breakdown, field enhancement associated with small deformations on the cavity surface results in electrical 
arcs. Such arcs degrade a passing beam and if they occur frequently, they can cause irreparable damage to the RF cavity 
surface. We propose a machine learning approach to predict the occurrence of breakdowns in CERN’s 
Compact LInear Collider (CLIC) accelerating structures. 
We use state-of-the-art algorithms for data exploration with unsupervised machine learning, breakdown prediction 
with supervised machine learning, and result validation with Explainable-Artificial Intelligence (Explainable AI). 
By interpreting the model parameters of various approaches, we go further in addressing opportunities to elucidate 
the physics of a breakdown and improve accelerator reliability and operation.

The structure of the CLIC XBOX 2 test stand set-up is shown bellow.
![CLIC XBOX 2 Test Stand](images/teststand.png)

## Contributors
* Christoph Obermair, TE-MPE-CB, TU-Graz
* Lorenz Fischl, TE-MPE-CB, TU-Vienna
* Thomas Cartier-Michaud, TE-MPE-CB
* Andrea Apollonio , TE-MPE-CB
* Lee Millar, CLIC BE-RF
* Franz Pernkopf, TU-Graz

## Machine Learning Framework

## Main Presentations
* [20201028: Presentation of Trenddata Results](presentations/20201028_CLIC_Trenddata_Results.pptx)
* [20201201: Presentation of Eventdata Results](presentations/20201201_CLIC_Eventdata_Results.pptx)
* [20201210: End of the Year Presentation](presentations/20201210_CLIC_Summary.pptx)

## Minutes

### 2020
* [20201210: End of the Year Presentation](minutes/20201210_End_of_the_year_presentation.txt)
### 2021
* [20210201: Timing Measurement](minutes/20210201_Timing_measurement.txt) [(slides)](presentations/20210201_Timing_measurement.pptx) 
* [20210216: Melbox Meeting](minutes/20210216_Melbox_meeting.txt) [(slides)](presentations/HG2019_CavBD_ABE_v2.pdf)
* [20210319: Pressure rise causes ideas](minutes/20210319_Pressure_rise.txt)[(thesis)](presentations/Thesis_rf_bds.pdf)
* [20210323: Experiments XBOX3](minutes/20210323_Experiments_Xbox3.txt) 

* [20210419: Involvement Software Team](minutes/20210419_Involvement_ST.txt) [(slides1)](presentations/20210419_CLIC_framework_general_information.pdf) [(slides2)](presentations/20210419_CLIC_framework_design_choices.pdf)
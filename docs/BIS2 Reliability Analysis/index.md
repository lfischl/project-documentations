# BIS2 Reliability Analysis

Reliability study of the Beam Interlock System version 2. 

This work is a quantitative reliability analysis of the upgrade of the Beam Interlock System version 2. The analysis will be carried out through the use of the software package Isograph. An acceptable failure rate has been determined with the team along with the identification of the main failure modes. A top-down approach will be carried out to define the reliability requirements for the system. This is to be followed by a bottom-up approach with Isograph; starting off with a prediction analysis, followed by an FMECA anaysis, and then a fault tree analysis. 

A page for the team to post meeting minutes and any relevant documentation.

## Minutes

### 2021
* [19022021: 1st Meeting](Minutes/BIS2_Reliability_190221.docx)
    - [Presentation](Minutes/BIS2_Reliability_Requirements_190221.pptx)
* [26022021: 2nd Meeting](Minutes/BIS2_Reliability_260221.docx)
    - [Presentation](Minutes/BIS2_Reliability_Requirements_260221.pptx)
